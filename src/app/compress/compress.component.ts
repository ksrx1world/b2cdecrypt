import { Component } from '@angular/core';

@Component({
  selector: 'app-compress',
  templateUrl: './compress.component.html',
  styleUrls: ['./compress.component.css'],
})
export class CompressComponent {
  plainText;
  compressedText;
  isHeadCompress = false;

  onCompressText() {
    var allHTML = this.plainText;
    var headHTML = '';
    var removeThis = '';
    var headstatus = this.isHeadCompress;

    if (headstatus != true) {
      //Compress all the things!
      allHTML = allHTML.replace(/(\r\n|\n|\r|\t)/gm, '');
      allHTML = allHTML.replace(/\s+/g, ' ');
    } else {
      //Don't compress the head
      allHTML = allHTML.replace(new RegExp('</HEAD', 'gi'), '</head');
      allHTML = allHTML.replace(new RegExp('</head ', 'gi'), '</head');
      var bodySplit = '</head>';
      var i = allHTML.indexOf(bodySplit) != -1;
      if (i == true) {
        var bodySplit = '</head>';
        let tempo = allHTML.split(new RegExp(bodySplit, 'i'));
        headHTML = tempo[0];
        allHTML = tempo[1];
      } else {
        bodySplit = '';
      }
      allHTML = allHTML.replace(/(\r\n|\n|\r|\t)/gm, '');
      allHTML = allHTML.replace(/\s+/g, ' ');
      allHTML = headHTML + bodySplit + '\n' + allHTML;
    }
    this.compressedText = allHTML;
  }

  onCopyToClipboard(){
    navigator.clipboard.writeText(this.compressedText).then(() => {
    }).catch(err => {
      console.error('Could not copy text: ', err);
    });
  }
}
