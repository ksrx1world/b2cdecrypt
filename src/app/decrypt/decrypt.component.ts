import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as CryptoJS from 'crypto-js';

import {
  FormGroup,
  FormControl,
  UntypedFormBuilder,
  Validators,
} from '@angular/forms';
import JSONFormatter from 'json-formatter-js';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-decrypt',
  templateUrl: './decrypt.component.html',
  styleUrls: ['./decrypt.component.css']
})
export class DecryptComponent {

  isJson: boolean = false;
  formattedData;
  jsonData;
  renderer: any;
  session_id = false;
  info;
  prettifyMode = false;

  constructor(private fb: UntypedFormBuilder, private element: ElementRef, private toastr: ToastrService) {}
  ngOnInit(): void {
    this.form.get('dualCheck').valueChanges.subscribe((res) => {
      if (res) {
        this.form.get('sessionId').reset();
      }
      this.session_id = res;
    });
    this.form.get('queryBox').valueChanges.subscribe((res) => {
      localStorage.setItem('b2cdecrypt', res || "");
      if (res) {
        // if(btoa(atob(res))==res){
        try {
          if(btoa(atob(res))==res){
            this.info = null;
             this.form.get('base64Check').setValue(true);
          }
        } catch (e) {
          this.info = "Not a Base64";

          this.form.get('base64Check').setValue(false);
        }
      }
    });

    this.tForm.get('toggleBox').valueChanges.subscribe((res) => {
      localStorage.setItem('b2cdecryptView', res);
      if (res) {
        this.form.reset();
      }
    });
  }
  decForm = this.fb.group({
    queryBox: [localStorage.getItem('b2cdecrypt') || "", Validators.required],
    base64Check: [true],
    dualCheck: [false],
    sessionId: [{ value: '' }],
    output: [''],
    jsonPrettify: [false],
  });
  toggleForm = this.fb.group({
    toggleBox: [localStorage.getItem('b2cdecryptView') || false, Validators.required],
  });

  decryptionWithCryptoJS(cipher: string, hkey?: string): string {
    const key = CryptoJS.enc.Utf8.parse(hkey);
    const iv1 = CryptoJS.enc.Utf8.parse(hkey);
    const plainText = CryptoJS.AES.decrypt(cipher, key, {
      keySize: 128,
      iv: iv1,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
    });
    return plainText.toString(CryptoJS.enc.Utf8);
  }

  isJsonType(item) {
    item = typeof item !== 'string' ? JSON.stringify(item) : item;

    try {
      item = JSON.parse(item);
    } catch (e) {
      return false;
    }

    if (typeof item === 'object' && item !== null) {
      return true;
    }

    return false;
  }

  prettifyJSON(depth?) {
    this.prettifyMode = true;
    this.removechild();
    // this.form.get('output').setValue(JSON.stringify(new JSONFormatter(this.formattedData)));
    // this.jsonData = new JSONFormatter(this.formattedData);
    const formatter = new JSONFormatter(JSON.parse(this.formattedData));
    this.jsonData = formatter.render();
    // this.element.nativeElement.appendChild(formatter.render());
    document.getElementById("mydiv").appendChild(formatter.render());
    formatter?.openAtDepth(depth);
  }

  resetForm() {
    this.isJson = false;
    this.formattedData = '';
    this.form.reset();
    this.removechild();
  }

  removechild() {
    if(document.getElementById("mydiv")?.lastChild){
      document.getElementById("mydiv").removeChild(document.getElementById("mydiv").lastChild)
    }
    
  }

  resetPrettify(){
    this.prettifyMode = false;
    this.removechild();
  }

  depth(){
    this.prettifyJSON(4);
  }

  copyText(){
    navigator.clipboard.writeText(this.formattedData);

  }

  submit() {
    // this.toastr.success('Hello world!', 'Toastr fun!');
    console.log("rer");
    this.removechild();
    // this.element.nativeElement.removeChild();
    let data = this.form.get('base64Check').value
      ? atob(this.form.get('queryBox').value)
      : this.form.get('queryBox').value;
    // this.decryptionWithCryptoJS(data);
    if (this.form.get('dualCheck').value) {
      let sessionId = this.form.get('sessionId').value?.substring(0, 16);
      data = this.decryptionWithCryptoJS(data, sessionId);
    }
    try{  
      this.formattedData = this.decryptionWithCryptoJS(data, '//htps12345nrfbw');
      this.info = null;
    }catch(err){

      this.info = "Dual Encryption required || Store Data"
    }
    this.isJson = this.isJsonType(this.formattedData);
    if(this.isJson){
      this.formattedData = JSON.parse(JSON.stringify(this.formattedData, null, "/t"));
    }
    this.form.get('output').setValue(this.formattedData);
    if(this.prettifyMode){
      this.prettifyJSON();
    }
  }

  get form() {
    return this.decForm;
  }

  get tForm() {
    return this.toggleForm;
  }
}
