import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompressComponent } from './compress/compress.component';
import { DecryptComponent } from './decrypt/decrypt.component';

const routes: Routes = [
  {path:'decrypt', component: DecryptComponent},
  {path: 'compress', component: CompressComponent},
  {path:'**', redirectTo: '/decrypt'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
